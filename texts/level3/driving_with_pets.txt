Driving with Pets.
Sandy Michelson thinks of her mini terrier,
Marshmallow, as a part of her family. �He�s my
baby,� she said with a smile, holding the small
dog in her arms. �I take him with me
everywhere.�
Sandy often drove her car with Marshmallow on
her lap. �He loved sticking his head out the
window. It was so cute.� She figured that it
was safe since he was so small. �I didn�t have to
reach around him too much to hold onto the
steering wheel. I figured if something
happened, I�d just grab him and hold him, and
he�d be fine.�
She was wrong.
On a Saturday afternoon last month, Sandy was
taking Marshmallow to the groomer�s. �It had
been rainy, and the roads were a little slick.
Marshmallow was on my lap as usual, and I put
one hand on his back to hold him still while I
tried to use the other hand to turn the car into
the parking lot. I didn�t notice that there was a
truck behind me.�
The truck�s driver didn�t realize Sandy had
slowed down and started to turn until it was too
late to stop on the slick roads. The truck hit the
back of Sandy�s car.
Sandy�s car flew forward. The airbag deployed,
preventing her from having a serious injury. �I
ended up with some cuts and scrapes, and a
mild case of whiplash.� Whiplash is damage to
the muscles in the back of the neck due to the
force of the sudden impact.
�But even as small as Marshmallow is, I couldn�t
hold on to him.� The airbag hit his back legs
and sent him flying into the side of the door.
Both of his back legs were broken.
�He�s recovering,� said Sandy, lightly stroking
the casts on Marshmallow�s legs. �But he�s in
pain a lot and can�t even go to the bathroom
without help. And all those veterinarian bills!
It�s going to take me a long time to pay for his
medical care.�
Sandy was given a ticket for failure to use a turn
signal.
Recently in the news, driving with distractions
has been a popular topic. Many states and
cities have passed � or are considering passing -
laws that make it illegal to send text messages
while driving. Some states have made it illegal
to talk on a cell phone while driving. Research
has repeatedly shown that drivers who are
distracted can be as impaired as someone
driving drunk.
But what about driving with pets?
�Unfortunately, we see it all the time,� says
Officer Terry Ferguson from the county sheriff�s
office. �People don�t seem to think anything is
wrong with having a pet unrestrained in their
car.�
No one is exactly sure how many accidents are
caused each year because of pets riding in
vehicles, but a recent study estimates that there 
are tens of thousands of these accidents every
year across the country.
�Pets can distract the driver in so many ways,�
explained Officer Ferguson. �The animal may
start barking or go to the bathroom or start
chewing on something. The driver�s attention
goes to the animal, which means the driver is
not paying attention to driving.�
In some cases, the pet just simply gets in the
way. �Maybe the driver can�t turn the steering
wheel easily because the pet is in the way, or
the pet blocks the driver�s view. In one local
case, a cat got scared by the horn on a passing
car and tried to hide under the car�s pedals on
the floor. The driver couldn�t break with the cat
under the pedal and ended up crashing into a
tree.
When a driver is behind the wheel, he or she
needs to do their very best to pay attention to
the road. One federal agency calculated that in
2009 over 5,400 people were killed in accidents
caused by a distracted driver. Almost 450,000
were injured.
Only one state, Hawaii, has a law that forbids
drivers from having an animal on their lap while
driving. Other states and cities are considering
similar laws.
�We did a big promotion last summer, handing
out pet restraint systems for free to interested
pet owners. Basically the system works like a
seatbelt for the pet. But I kept hearing people
giving all sorts of excuses as to why they didn�t
think they needed it. People said their pets
were well behaved or that the animal wouldn�t
like it or that their pet was so small that there
was no point.�
In reality, an unrestrained animal in a car that
stops suddenly flies through the vehicle like a
cannon ball. In a car moving 35 miles an hour, a
50 pound animal would be thrown forward with
1,500 pounds of force.
�Last summer in another part of the state, there
was an accident when a dog riding in the back
of a pickup truck was killed from the impact of a
crash. It was a tragedy, but it could have been
easily prevented,� said Officer Ferguson.
�I never go anywhere now with Marshmallow
without having his safety restraint on in the car.
At first he whined about it, but I told him it was
for his own good,� said Sandy. �I love him too
much not to try to keep him safe.�