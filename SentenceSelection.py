#requires an array containing the words a sentense should contain
def findSentencesContaining(text, requiredwords):
	result = [];
	for sentence in text['sentences']:
		foundwords = [];
		for word in sentence['tokens']:
			if word['originalText'] in requiredwords:
				foundwords.append(word['originalText']);
		
		valid = True;
		for searchword in requiredwords:
			if not searchword in foundwords:
				valid = False;
				break;
		
		if valid:
			fullsentence = "";
			for word in sentence['tokens']:
				fullsentence += word['before'] + word['originalText'] + word['after'];
			result.append(fullsentence);
	return result;
	
def ScoreSentenceContains(sentence, word):
	returnval = 0;
	if word in sentence:
		returnval = 1;
	return returnval;
	