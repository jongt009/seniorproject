Beat the Heat.
It�s hot! Summer sunshine brings hot temperatures
to many places. Some people love the heat. It can
be a great time for people who love to go boating or
swimming. The heat is great for those who just love
to sit in the shade with some cold lemonade.
For many people, however, hot weather causes a
lot of problems. In the last several years, many
records have been set for hot temperatures. In
places like northern Europe and Russia, hot weather
is rare. People who live there are often not ready
to deal with the heat when it happens.
It�s Cooler in the Shade
How hot it gets outside depends on many things.
Latitude and altitude are two things that make a big
difference in how hot a place gets.
Latitude describes how far north or south a place
is. The equator is the line around the middle of the
Earth. This area gets the most direct sunlight, so
temperatures in those areas tend to be higher.
Temperatures tend to be colder as you move north
toward the North Pole or south toward the South
Pole. These places get much less direct sunlight and
therefore less heat.
Altitude describes how high a location is compared
to sea level. Temperatures tend to be hottest
closest to sea level and cooler further up
mountains. In many places it is possible to have
summer temperatures in the 80�s and 90�s in a
valley and to still have snow on the mountain tops
nearby. Ecuador is a country in South America
which the equator runs right through. In Ecuador,
there are mountains so tall that they have glaciers
on top all year long. But the lower areas of the
country are hot tropical forest.
Large cities tend to have higher temperatures than
more rural areas. This is because the concrete and
roads in the city absorb the sun�s heat during the
day and hold on to it. This makes it harder for the
area to cool down during the night. Large desert
cities like Phoenix will often have temperatures in
the summer that don�t drop below 90: even at
night.
Areas with a lot of trees and green plants can be as
much as 5-14 degrees cooler than their concrete
neighbors. Shade makes a big difference.
Researchers are looking for ways to add more shade
to city streets. One way to increase shade is to
plant more trees. In places where there�s not room
for a tree, a canopy on the side of the building or
over the sidewalk can help.
Weather Extremes
The hottest temperature ever recorded on Earth
was 136: on September 13, 1922 in Libya, a desert
country in northern Africa. In the United States,
Death Valley in California holds several records for
heat. The hottest recorded temperature there was
134: on July 10, 1913. Death Valley has the hottest
summer temperatures in the Western Hemisphere.
It is the only place in the U.S. where the
temperature overnight in the summer can be over
100:.
The average summer temperature in Death Valley is
also the highest in the nation at 92.8:. The highest
average temperature in the winter, however, is in 
Honolulu, Hawaii. The average temperature there
is 72.8:. Hawaii is the most southern state. It is
warm year-round. Ocean breezes keep it from
being too hot, however. The highest record
temperature in Hawaii is 100: - the same as the
highest recorded temperature in Alaska.
Humans tend to be most comfortable in
temperatures between 68: and 80:. As the
temperature gets higher, the heart has to work
harder. People begin to feel tired and grouchy. It
becomes harder to concentrate. The body begins to
attempt to cool itself down by sweating.
Temperatures between 95: and 104: are
considered the hottest that humans can deal with
easily. Loss of the body�s water due to sweating
can lead to heat exhaustion. It can cause muscle
cramping. Dizziness and fainting may also happen.
Heat rash and swelling are also common. This
happens more for people who are not used to being
around high temperatures.
Heat stroke is the most serious heat-related illness.
The body temperature rises above 105:. The
person stops sweating. The victim may faint.
Immediate medical help is required. Without
medical help, the victim may suffer damage to the
brain, kidneys and heart. Heat stroke can be fatal.
Those most at risk include young children, those
who have chronic illness, and the elderly.
Between 1999-2003 in the United States, 3,442
people died of heat-related illnesses. The most
deaths happened in Arizona and Nevada. 40% of
those who died from the heat were over the age of
65. 7%, just over 200 people, were under the age of
15.
How to Beat the Heat
Not many people lived in the American southwest
until technology such as air conditioners made it
more comfortable to live in the desert heat. Electric
fans and air conditioning are great ways to stay cool
and healthy in the hottest times of the year. Here
are some other ways to keep your cool:
 - Spend time outside in the mornings. You may
have outside chores to do. You may have to mow
the lawn or weed the garden. Get outdoor
activities done during the coolest part of the day.
 - Use the shade. Plant trees around your
house. Shade from the trees keeps the walls and
roof from heating up. Keep window shades and
curtains closed so that less heat comes into your
house.
 - Drink water or energy drinks containing
electrolytes. Drink throughout the day to keep your
body hydrated. Limit sugary drinks. Limit drinks
with caffeine.
 - At night, turn the fan around so that it blows
air out the window. This will force out the heated
air that is trapped in your house during the day.
 - Limit use of electrical devices. Items such as
computers, TVs, and stereos create heat. Keeping
them turned off can lower the temperature in a
room by a degree or two.