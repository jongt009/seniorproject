# coding=utf-8

import json;
import os

class Settings:

	settingFile = "settings.txt";
	
	settingsDictionary = None;
	settingsDictIndex = {};
	current = None;
	
	def __init__(self, settingsfile = ""):
		if settingsfile != "":
			Settings.settingFile = settingsfile;

	
	def loadSettings(self):
		file = open(Settings.settingFile, encoding="utf-8", errors='ignore')
		allsettings = file.read();
		file.close()
		self.settingsDictionary = json.loads(allsettings)

	#returns true if current settings are valid
	def nextSetting(self):
		if self.settingsDictionary == None:
			self.loadSettings();
		if Settings.current == None:
			temp = {};
			for key, value in self.settingsDictionary.items():
				temp[key] = value[0];
				self.settingsDictIndex[key] = 0;
			Settings.current = temp;
			return True;
		
		#switch to next settings
		passed = [];
		for key, value in self.settingsDictionary.items():
			if len(value) - 1 == self.settingsDictIndex[key] :
				passed.append(key)
				continue;
			self.settingsDictIndex[key] += 1;
			break;
		for key in passed:
			self.settingsDictIndex[key] = 0;
		
		temp = {};
		lastKey = "";
		for key, value in self.settingsDictionary.items():
			temp[key] = value[self.settingsDictIndex[key]];
			lastKey = key;
		Settings.current = temp;
				
		return lastKey not in passed;

