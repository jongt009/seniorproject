# coding=utf-8

from tfidf import *

import time

import sys
import os
import operator
import json
import re
import ast
import math

import nltk, re, pprint

from nltk.tree import *

from TreeManipulation import *
from readfile import *
from settings import *
from SentenceSelection import *

from nltk.stem.snowball import SnowballStemmer

from pycorenlp import StanfordCoreNLP
nlp = StanfordCoreNLP('http://localhost:9000')

def run():
	text = getFileContents(Settings.current['filedir'] + Settings.current['file']);
	discoursemarkers = ast.literal_eval(getFileContents(Settings.current['discoursemarkers']));
	gradedtexts = getFileContents("datafiles/gradedtexts/" + Settings.current['filedir'] + Settings.current['file']);
	gradedtexts = gradedtexts.splitlines();
	
	methodResults = {};
	
	text = text.replace(" \n", " ");
	text = text.replace("\n ", " ");
	text = text.replace("\n", " ");
	
	starttime = time.perf_counter()
	
	preparse = nlp.annotate(text, properties={
		'annotators': 'dcoref',
		'outputFormat': 'json',
		'timeout': '500000' #500 seconds to parse
		});
	
	text = getTextCoreference(preparse);
		
	stopwords = getFileContents(Settings.current['stopwords']);
	# remove stop words
	simplifiedtext = ' '.join(filter(lambda x: x.lower() not in stopwords,  text.split()))
	
	parsedtextsimplified = nlp.annotate(simplifiedtext, properties={
		'annotators': 'tokenize,ssplit,pos,parse,ner',
		'outputFormat': 'json',
		'timeout': '500000' #500 seconds to parse
		});
	
	######### OLD METHOD ONLY #########
	parsedtext = nlp.annotate(text, properties={
		'annotators': 'tokenize,ssplit,pos,parse,ner',
		'outputFormat': 'json',
		'timeout': '500000' #500 seconds to parse
		});
	######### END OLD METHOD ONLY ####
	
	
	endtime = time.perf_counter()
	print("server side time", endtime - starttime, "\n");
	
	starttime = time.perf_counter()
		
	parsedata = ['documentroot'];
	for sentence in parsedtextsimplified['sentences']:
		parsedata.append(extractDataFromParse((sentence['parse'])));
		
	grammar = re.compile(Settings.current['regex']);
		
	grammarWordCount = getWordCountRegex(parsedata, grammar, "", {});
	
	sortedbygrammar = sorted(grammarWordCount.items(), key=operator.itemgetter(1), reverse=True)

	namedEntities = getNamedEntities(parsedtextsimplified, Settings.current['acceptedNER']);
	sortedNEs = sorted(namedEntities.items(), key=operator.itemgetter(1), reverse=True)
	
	
	############### UNUSED ##################
	#get verbs
	grammar = re.compile("<VB>|<VBD>|<VBN>|<VBP>|<VBG>|<VBZ>");
	verbs = getWordsRegex(parsedata, grammar, "", []);
	stemmer = SnowballStemmer("english");
	
	verbcount = {};
	for verb in verbs:
		key = stemmer.stem(verb);
		if key in verbcount:
			verbcount[key].append(verb);
		else:
			verbcount[key] = [verb];

	grammarindex = -1;
	NEindex = 0;
	
	################# END UNUSED ##############
	
	################# MITKOV ##################
	#grammar = re.compile('');
	
	generalWordCount = getWordCountRegex(parsedata, grammar, "", {});
	
	sortedGeneralWordcount = sorted(generalWordCount.items(), key=operator.itemgetter(1), reverse=True)
	
	result = [];
	
	sentenceindexes = getSentenceIndexesWithNP(parsedata);
	headsentences = [];

	for index in sentenceindexes:
		nextsentence = False;
		for noun in sortedGeneralWordcount[0:2]:
			if hasNPHead(parsedata[index + 1], noun[0]):
				headsentences.append(index);
				continue;
	headsentences = list(set(headsentences));
	
	# taken from https://brenocon.com/JustesonKatz1995.pdf page 16 - 17
	# Adjective = JJ
	# Noun = NN
	regex = re.compile( "((<JJ>|<NN>)+<NN>)|((<JJ>|<NN>)*(<NP>)?(<JJ>|<NN>)*<NN>)");
	
	mitcofresult = [];
	
	for index in headsentences:
		sentenceregex = getSentenceRegex(parsedata[index + 1])
		found = re.search(regex, sentenceregex);
		if found:
			mitcofresult.append(index);
			
	print (len(headsentences));
	
	
	
	result = [];
	for index in headsentences:
		#print(gradedtexts[index]);
		result += gradedtexts[index][0];
	
	methodResults['headsentences'] = result;
		
	print (len(mitcofresult));
	
	result = [];
	for index in mitcofresult:
		#print(gradedtexts[index]);
		result += gradedtexts[index][0];
	
	methodResults['mitkov'] = result;
	
	################# END MITKOV ###############
	
	################# OLD METHOD ###############
	result = [];
	while (grammarindex != 2 or NEindex != 2) and len(result) < 5:
		if sortedbygrammar[grammarindex][0] == sortedNEs[NEindex][0]:
			continue;
			
		if grammarindex > NEindex:
			NEindex += 1;
		else:
			NEindex = 0;
			grammarindex += 1;
				
		result += findSentencesContaining(parsedtext, [sortedbygrammar[grammarindex][0], sortedNEs[NEindex][0]]);
	
	grammarindex = -1;
	while len(result) < 5 and grammarindex < 3:
		grammarindex += 1;
		
		for namedEntity in sortedNEs:
			result += findSentencesContaining(parsedtext, [sortedbygrammar[grammarindex][0], namedEntity[0]]);
			if len(result) >= 5:
				break;
	
	#remove duplicates (set cannot have duplicates)
	result = list(set(result))
	
	newresult = [];
	for r in result:
		newresult.append(r.replace('\n', "").replace("   ", " ").replace("  ", " "));
	
	endtime = time.perf_counter()
	print("time", endtime - starttime)
	
	#writeToFile("output/"+ Settings.current['file'], '\n'.join(newresult) + "\n--------------------- old end\n");
	
	################# END OLD METHOD ###########

	
	################## NEW SCORING SYSTEM ######
	
	sentences = [];
	scores = [];
	# get all sentences
	for sentence in parsedtext['sentences']:
		fullsentence = "";
		for word in sentence['tokens']:
			fullsentence += word['before'] + word['originalText'] + word['after'];
		sentences.append(fullsentence.replace('\n', "").replace("   ", " ").replace("  ", " ").strip());
		scores.append(scores);
	
	for i in range(0, len(sentences)):
		sentence = sentences[i];
		scoring = scores[i];
		
		commonWordsScore = 0;
		numInList = 0;
		for word in sortedbygrammar:
			# only check top 5 items
			if numInList > 4:
				break;
			
			commonWordsScore += ScoreSentenceContains(sentence, word[0]) * (1 / (1 + numInList));
			numInList += 1;
		NamedEntitiesScore = 0;
		numInList = 0;
		for NE in sortedNEs:
			# only check top 5 items
			if numInList > 4:
				break;
			
			NamedEntitiesScore += ScoreSentenceContains(sentence, NE[0]) * (1 / (1 + numInList));
			numInList += 1;
		
		lenghtscore = 0;
		wordcount = sentence.count(' ');
		if wordcount < 8 or wordcount > 12:
			lenghtscore = math.fabs(10 - wordcount) * - 0.2;
		
		tfidfscore = 0;
		words = sentence.split();
		for word in words:
			tfidfscore += getScore(word);
		
		discourseScore = 0;
		for marker in discoursemarkers:
			if marker in sentence:
				discourseScore += 1;
		
		humanscore = gradedtexts[i][0]
		
		#sentence id, scores
		scores[i] = [i, humanscore, commonWordsScore, NamedEntitiesScore, lenghtscore, tfidfscore, discourseScore];
		
	sortedset = sorted(scores, key=lambda score: score[2] + score[3] + score[4] + score[5] + score[6], reverse = True);	
	
	
	result = [];

	scoredResult = [];
	for scoreset in sortedset[0:5]:
		sentence = sentences[scoreset[0]];
		scoredResult.append(sentence + str(scoreset));
		result += gradedtexts[scoreset[0]][0]
	
	methodResults['mine'] = result;
	
	writeToFile("output/"+ Settings.current['file'], '\n'.join(scoredResult) + "\n--------------------- scored end \n");
	
	writeToFile("output/rawresults.txt", "\n--------------------- " + Settings.current['filedir'] + Settings.current['file'] + "\n" + str(methodResults));
	
	totalOccurence = { '0':0, '1':0, '2':0 };
	for sentence in gradedtexts:
		totalOccurence[sentence[0]] += 1;
	
	print(totalOccurence);

	PrintForFile = "\n--------------------- " + Settings.current['filedir'] + Settings.current['file'] + "\n";
	for key in methodResults.keys():
		scorearray = methodResults[key];

		setOccurence = { '0':0, '1':0, '2':0 };
		for val in scorearray:
			setOccurence[str(val)] += 1;
		
		precision2 = 0;
		precision1 = 0;
		precisionTotal = 0;
		if len(scorearray) > 0:
			precision2 = 100 * setOccurence['2'] / len(scorearray) ;
			precision1 = 100 *setOccurence['1'] / len(scorearray);
			precisionTotal = 100 * (setOccurence['1'] + setOccurence['2']) / len(scorearray);
		
		recall2 = 100 * setOccurence['2'] / totalOccurence['2'];
		recall1 = 100 * setOccurence['1'] / totalOccurence['1'];
		recallTotal = 100 * (setOccurence['2'] + setOccurence['1']) / (totalOccurence['2'] + totalOccurence['1']);
		
		PrintForFile += "\nMethod: " + key;
		PrintForFile +=	"\nPrecision 2: " + '%.1f' % precision2 + " Recall 2: " + '%.1f' % recall2;
		PrintForFile +=	"\nPrecision 1: " + '%.1f' % precision1 + " Recall 1: " + '%.1f' % recall1;
		PrintForFile +=	"\nPrecision T: " + '%.1f' % precisionTotal + " Recall T: " + '%.1f' % recallTotal;
		
	writeToFile("output/processedresults.txt",  PrintForFile);
	