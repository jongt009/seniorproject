0Beat the Heat.
0Beat the Heat.
0hot!
0Summer sunshine brings hot temperatures to many places.
0Some people love the heat.
0the Heat can be a great time for people who love to go boating or swimming.
0The heat is great for those who just love to sit in the shade with some cold lemonade.
0For many people, however, hot weather causes a lot of problems.
0In the last several years, many records have been set for hot temperatures.
1In places like northern Europe and Russia, hot weather is rare.
0People who live there are often not ready to deal with the heat when the Heat happens.
0the Heat Cooler in the Shade How hot the Heat gets outside depends on many things.
2Latitude and altitude are two things that make a big difference in how hot a place gets.
2Latitude describes how far north or south a place is.
2The equator is the line around the middle of the Earth.
0This area gets the most direct sunlight, so temperatures in those areas tend to be higher.
1temperatures in those areas tend to be colder as you move north toward the North Pole or south toward the South Pole.
0These places get much less direct sunlight and therefore less heat.
2Altitude describes how high a location is compared to sea level.
0temperatures in those areas tend to be hottest closest to sea level and cooler further up mountains.
0In many places it is possible to have summer temperatures in the 80s and 90s in a valley and to still have snow on the mountain tops nearby.
1Ecuador is a country in South America which the equator runs right through.
0In Ecuador, there are mountains so tall that they have glaciers on top all year long.
0But the lower areas of the country are hot tropical forest.
1Large cities tend to have higher temperatures than more rural areas.
0This is because the concrete and roads in the city absorb the suns heat during the day and hold on to the city.
0This makes This harder for the area to cool down during the night.
2Large desert cities like Phoenix will often have temperatures in the summer that dont drop below 90: even at the night.
1Areas with a lot of trees and green plants can be as much as 5-14 degrees cooler than their concrete neighbors.
0Shade makes a big difference.
0Researchers are looking for ways to add more shade to city streets.
0One way to increase shade is to plant more trees.
0In places where theres not room for a tree, a canopy on the side of the building or over the sidewalk can help.
1Weather Extremes The hottest temperature ever recorded on the Earth was 136: on September 13, 1922 in Libya, a desert country in northern Africa.
1In the United States, Death Valley in California holds several records for heat.
2The hottest recorded temperature there was 134: on July 10, 1913.
2Death Valley has the hottest summer temperatures in the Western Hemisphere.
0It is the only place in the U.S. where the temperature overnight in the summer can be over 100:.
2The average summer temperature in Death Valley is also the highest in the nation at 92.8:.
2The highest average temperature in the winter, however, is in Honolulu, Hawaii.
0The average temperature there is The average temperature there:.
1Honolulu, Hawaii is the most southern state.
0Honolulu, Hawaii is warm year-round.
1Ocean breezes keep Honolulu, Hawaii from being too hot, however.
1The highest record temperature in Honolulu, Hawaii is 100: - the same as the highest recorded temperature in Alaska.
2Humans tend to be most comfortable in temperatures between 68: and 80:.
0As the temperature gets higher, the heart has to work harder.
0People who live there begin to feel tired and grouchy.
0It becomes harder to concentrate.
0The body begins to attempt to cool The body down by sweating.
1Temperatures between 95: and 104: are considered the hottest that Humans can deal with easily.
0Loss of the bodys water due to sweating can lead to heat exhaustion.
0Loss of the bodys water due to sweating can cause muscle cramping.
0Dizziness and fainting may also happen.
0Heat rash and swelling are also common.
0This happens more for people who are not used to being around high temperatures.
2Heat stroke is the most serious heat-related illness.
0The body temperature rises above 105:.
0The person stops sweating.
0The victim may faint.
0Immediate medical help is required.
0Without medical help, the victim may suffer damage to the brain, kidneys and heart.
1Heat stroke can be fatal.
0those who just love to sit in the shade with some cold lemonade most at risk include young children, those who have chronic illness, and the elderly.
2Between 1999-2003 in the United States, 3,442 people died of heat-related illnesses.
0The most deaths happened in Arizona and Nevada.
040% of those who died from the heat were over the age of 65.
07%, just over 200 people, were under the age of 15.
0How to Beat the Heat Not many people lived in the American southwest until technology such as air conditioners made Beat the Heat.
0more comfortable to live in the desert heat.
0Electric fans and air conditioning are great ways to stay cool and healthy in the hottest times of the year.
0Here are some other ways to keep your cool: - Spend time outside in the mornings.
0You may have outside chores to do.
0You may have to mow the lawn or weed the garden.
0Get outdoor activities done during the coolest part of the day.
0- Use the shade.
0Plant trees around your house.
0Shade from the trees keeps the walls and roof from heating up.
0Keep window shades and curtains closed so that less heat comes into your house.
0- Drink water or energy drinks containing electrolytes.
0Drink throughout the day to keep your body hydrated.
0Limit sugary drinks.
0Limit drinks with caffeine.
0- At the night, turn the fan around so that Limit sugary drinks.
0blows air out the window.
0This will force out the heated air that is trapped in your house during the day.
0- Limit use of electrical devices.
0Items such as computers, TVs, and stereos create heat.
0Keeping This turned off can lower the temperature in a room by a degree or two.