Buried Treasure.
John Crawford wasn�t looking for buried
treasure when he started digging in a remote
corner of his farm. He just wanted to put in a
new fence.
When John�s shovel hit the wooden box buried
underground, his first thought wasn�t that he�d
found something valuable. He was just angry
because he didn�t want any delays getting the
fence finished.
John�s wife, JoAnne, had been nagging him to
get the fence finished for weeks. They had a
new Border collie puppy, Chewy. Chewy liked
to run and loved living on the farm where there
was a lot of space. But Chewy had gotten off
the farm several times and was bothering the
neighbors� animals. JoAnne was concerned that
Chewy would get lost or hurt. So she began
asking John, every day, to add a section of fence
along a back area of the farm. The area was
wooded, so it had never been fenced, but John
made a plan for where he could dig holes for
fence posts and put up a fence that would work
around the trees.
John and JoAnne Crawford had lived on the
farm since 1986. They grew some small crops,
including corn and berries, but mostly they had
wanted a farm to give their four children a lot of
space to run around and play while they were
growing up. Their youngest son, Brian, now age
22, still lives in the area and was helping his
father dig holes for the new fence.
When John Crawford�s shovel struck a hard,
solid object under the dirt, he wanted to quit
digging and look for a way to move the fence
hole to someplace easier to dig. Brian
encouraged him to find out what was buried
there.
�The shovel had made a thud kind of sound, not
like the clank you hear when the shovel hits a
rock. It could have just been the root of a tree,
but I thought it was worth checking out.�
They had moved only a few shovelfuls of dirt
before they realized the object was some kind
of wooden crate.
�We completely forgot about building the fence
and focused completely on figuring out what
was in that box,� John said with a laugh. �We
started making bets about what was inside.�
�I guessed it was left-over construction
materials, nails or something, since the box
seemed kind of heavy,� explained Brian.
�I figured it was just trash. I remember my
grandparents telling me that back in their day,
before there were garbage trucks that would
drive around picking up people�s trash, people
would burn most of their trash and bury
whatever couldn�t be burned. So when I saw
the box, I just thought it was some old trash.�
It took two hours to uncover the top of the box
and to clear enough space around the edges to
be able to get the top off of the crate.
Using the tip of one shovel, John was able to
slowly pry the lid off the top of the crate. Inside
the box they saw layers of old newspaper.
�The newspapers were dated from April of
1983, which means that the box was buried just
a few years before we bought this farm,� John
explained. �So it wasn�t really all that old.�
Brian pulled the first object out of the crate. �It
was a rectangular block, wrapped in another
piece of newspaper. I was surprised at how
heavy it was, a few pounds at least.�
But that surprise was nothing compared to the
surprise they got when they took off the
newspaper wrapping.
�It was tarnished, but there was no doubt as to
what it was,� John shook his head and laughed.
�I was standing in the back corner of a farm
holding a bar of silver in my hands.�
And there wasn�t just one bar. There were 25
of them.
Each bar measured about 3 inches wide and a
little more than 5 inches long. Each bar was
stamped with numbers to indicate its weight:
50 ounces.
The value of silver is based on its weight and
fluctuates with the stock market. Currently,
one ounce of silver is worth a little more than
$27.
That means that one bar was worth around
$1350.
The total value of all twenty-five bars was
around $33,750.
�That�s more money than I make working at my
job for an entire year!� laughed Brian. �Enough
for a really nice new car.�
�Enough to take the entire family on a great
vacation,� sighed John. �But I can�t stop
wondering about who the silver originally
belonged to, what it was for and why it was
buried here.�
Legally the silver belongs to John and his wife
because they bought the land; there�s an
expectation that when land is purchased, they
get what comes along with it. �We bought the
farm and the house and that includes all the
trees and plants on the property, all the rocks.
And these silver bars too. It just feels kind of
strange to keep so much money when we know
it wasn�t meant to be ours.�
John and his wife are doing some research to
track down who was living on their farm in
1983, the date on the newspapers. �That�s
probably when the bars were buried,� John
explained. �We need to at least make an effort
to find out who the silver belonged to.� But
they won�t be giving it all back.
�We thought maybe we�d offer them half.
Doesn�t that seem fair?� asked John.
In the meantime, John and Brian have a fence
that needs to be finished.