Healthy Potatoes.
Greg Daniels loves potatoes.
Greg grew up on a potato farm. He�s been
surrounded by potatoes for most of this life. He
went away for college, but he came back home
soon after graduation. Now he teaches high
school science and horticulture classes, and he
spends weekends and summers helping his
father on the family potato farm, just like he did
when he was a kid. His goal is to help his
students understand and appreciate the
agricultural roots of the area in which they live.
Recently there have been news reports and
television shows reporting on the quality of
school lunches. According to federal guidelines,
French fries count as a serving of vegetables.
Many parents and others were upset that a
salty, deep-fried food is replacing healthier
options like carrot sticks or cucumbers. As a
result, the potato � what French fries are made
of � has been getting a bad rap.
�The problem is not the potato,� insists Greg.
�Potatoes are very healthy. But any food, even
carrots or lettuce, will be less healthy if it�s
cooked in grease.�
An order of French fries can have 500 calories
or more, and over 25 grams of fat. That�s half
of the fat that an adult should eat for an entire
day.
A medium-sized plain baked potato has 170
calories and less than 1 gram of fat. Potatoes
are also high in fiber, which helps a person feel
full and helps their digestive system work more
effectively. They have vitamins C and B and are
a great source of potassium, a nutrient which
helps maintain heartbeat and regulates fluid
levels throughout the body.
�Potatoes are a healthy choice if they�re
prepared correctly,� says Greg. �Frying the
potatoes or piling on butter and sour cream just
add fat, but there are lots of ways potatoes can
be prepared that keeps them healthy and that
taste fantastic.�
To prove his point, Greg has made a pledge to
eat nothing but potatoes for sixty days. �I want
to draw attention to how healthy potatoes
really are. I�m not going to eat any French fries,
and I�m not going to load my potatoes up with
toppings. I want to savor the taste of the
potato.�
That was three weeks ago. Now 21 days into
his 60 day plan, Greg admits he�s getting a little
tired of the tuber.
�This morning I had hash browns for breakfast,
mashed potatoes seasoned with ranch dressing
mix for lunch and red potatoes broiled with
olive oil and chives for dinner. Everything
tasted great, but I do miss having some variety.
Mostly I miss having dessert. I eat a piece of
potato every night after dinner and pretend it�s
a piece of chocolate.�
Greg�s pleased with the attention that he�s
getting � and the attention the potato is getting
� as a result. He�s writing a daily blog about
the experience. He�s been interviewed for
newspaper stories and appeared on the local
morning news last week.
�Potatoes are good. I want everyone to eat
them!� Greg says with a smile. �But I think
maybe 30 days of potatoes would have been
enough.�
Healthy Potatoes � After
�Well, I made it,� says Greg Daniels with a
smile.
Two months ago, Greg made a pledge to eat
nothing but potatoes for 60 days. As a high
school science teacher and son of a potato
farmer, he wanted to do something to draw
attention to potatoes as a healthy food and a
good choice to be part of anyone�s regular
meals.
�It�s been an interesting experience,� Greg said.
�For a while, I�ll admit I wasn�t sure I would
make it. I was pretty tired of eating potatoes
after a few weeks.�
But for Greg there were some benefits that he
hadn�t expected. He�s lost 15 pounds and says
he feels better than he has in years. �I have so
much more energy,� he says. �I�ve been
running and playing on a local basketball team,
and I feel like I�m playing as well as I did when I
was in high school.�
And Greg has gotten a lot of attention, much
more than he expected. �It�s been fantastic.
Thousands of people have read my blog. I�ve
been sharing recipes and talking about the
experience. Every day I get to tell people what
a great idea it is to eat potatoes.�
Greg and his wife were also interviewed in their
home for a national morning news show
watched by millions of people every day.
Greg ate mostly russet potatoes, the brown
potatoes most commonly used for baking.
These are the potatoes grown on his family�s
farm.
�We got creative too,� says Greg,� to try to get
some variety. I�ve had Yukon Gold potatoes,
red potatoes, even these amazing blue
potatoes. I�ve had them baked, mashed,
boiled, broiled and roasted. I�ve had hash
browns for breakfast almost every day. My wife
and I also tried some new recipes like latkes, a
kind of potato pancake. We�ve tried potato
dumplings, homemade baked potato chips, and
Irish potato soup.�
Greg says that it�s hard for him to pick a
favorite. �The only thing I tried and didn�t like
was lemon juice on potatoes. Oh, and one time
I tried a little chocolate sauce on a potato
because I really, really wanted a dessert, but
that was not a good combination.�
Greg is glad to be back to a normal diet, but he
still eats potatoes at least 5 or 6 times a week.
�Everyone should!� he says with a smile.
�Potatoes are healthy and taste great!�
 