0Barefoot Running.
0Barefoot Mike gets a lot of attention when Barefoot Mike shows up at the beginning of a race.
0Barefoot Mike loves to run.
0Hes been running since Barefoot Mike was a kid.
0Barefoot Mike loves to race.
2Barefoot Mike often competes in 10K races and half-marathons.
0But thats not the reason Barefoot Mike gets so much attention.
1People notice Barefoot Mike because when Barefoot Mike runs, Barefoot Mike runs barefoot.
0Running barefoot has become a huge trend in the world of running.
0More and more people are trying a huge trend.
0More and more people love the feeling of freedom.
0More and more people love the increased sensation that comes from getting rid of the shoes.
0I didnt set out to be a trend setter, laughed Barefoot Mike.
2Barefoot Mike is really Mike Whitaker.
0Ive always been a runner.
0But until a few years ago, I always wore the shoes.
0Then one day I was running after a rainstorm.
0I stepped off into the grass to avoid a puddle on the sidewalk.
0I accidentally stepped into a huge muddy puddle that had been hidden in the grass.
0I foot was wet up to I ankle.
0I tried to keep running, but the soggy shoe made the soggy shoe really hard to get any bounce off that foot.
0The wet, squishing feeling was just disgusting.
0I was several miles from home, so I couldnt stop.
0I figured the best solution was to just get rid of the shoes.
0Barefoot Mike took off Barefoot Mike shoes and dropped them into the nearest trash can.
0Then I started running.
0At first I went really slowly.
0I was watching out for rocks or other things could hurt I feet.
0But there really werent any problems.
0But there really werent any problems.
0started going faster and then ran at But there really werent any problems.
0usual pace.
0his usual pace felt so good!
0Barefoot Mike said.
0I knee, which always ached, didnt ache.
0I felt like I was getting farther with each stride.
0I foot was able to spread out so I had better balance.
0And I feet didnt feel all hot and sweaty.
0My foot was awesome!
0There are actually potential health benefits to running barefoot, said Dr. Nancy Bower, a podiatrist.
1A podiatrist is a doctor who specializes in treating feet and ankles.
0Most people assume that Most people need some sort of support or cushioning when doing an exercise like running.
0But really, very few people actually need the support shoes provide.
0In fact, wearing shoes may actually increase the chance of injury.
0A recent study agrees.
0A team of researchers in the universitys Sports Medicine program recently videotaped people running.
0researchers in the universitys Sports Medicine program recorded the same people running with the shoes and then barefoot.
0When wearing the shoes, the runners tended to land on the heel of the runners foot.
0This is called heel-striking.
0When the heel hits the ground first, there is a jarring impact along the bones in the lower leg.
0The impact reaches into the knee and up to the hip.
0Many runners have impact injuries such as shin splints or knee pain.
0Heel-striking may be the cause of these injuries.
0Or the cause of these injuries may make an existing injury worse.
0When running barefoot, the runners tended to land on the ball of the foot or in the middle of the foot.
0The foot is designed to handle impact in this manner.
0Instead of jarring impact, the force is spread out throughout the foot.
0The ankle, shin, knee and hip dont get the shock of a hard landing with every step.
0If you think about the shock of a hard landing with every step, said Barefoot Mike, the way the foot works is pretty amazing.
1A foot has something like 26 bones.
1A foot has 33 joints and more than 100 muscles, tendons and ligaments.
1A foot made to move and to support us.
0Were born barefoot.
0But us spend us lives wearing the shoes.
0Why?
0Dr. Bower has seen some patients with cuts on some patients feet as a result of running barefoot.
0a result of running barefoot really not as common as you would think.
0When running barefoot, the muscles in the foot have a chance to get stronger.
0This means that the pressure from a step doesnt all hit at one spot.
0The pressure is spread out.
0the pressure from a step doesnt possible for a runner to step on a sharp rock and not have a sharp rock cut the skin.
0Theres not enough pressure in that one spot for the skin to break.
0Calluses help too.
0The skin builds up a thick layer to help protect the skin.
0Many runners are interested in the freedom of barefoot running.
0But Many runners arent willing to be completely barefoot.
1The shoe industry is coming up with ideas.
0Several brands of barefoot shoes are now for sale.
0These shoes can be described as gloves for feet and ankles.
0They provide a thin, flexible layer of protection.
0They have divisions for each toe.
0They have vents to allow air to flow over the foot.
0Not everyone is a fan of barefoot running.
00People tell I all the time how gross My foot is that I run barefoot, said Barefoot Mike.
0That doesnt make any sense to I.
0A foot in a shoe will get all sweaty and smelly.
0A bare foot doesnt.
0How is that gross?
0Barefoot Mike hasnt yet entered a race that wont allow Mike hasnt to run barefoot.
0But Mike hasnt keeps some flipflops in the car for after the race.
1We go out to dinner to celebrate.
0I havent found many restaurants that allow I to eat barefoot.
0I dont understand why, but I go along with dinner to celebrate.
0Maybe someday the whole world will go barefoot.