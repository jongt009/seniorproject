#returns the contents of a file
def getFileContents(filename):
	file = open(filename, encoding="utf-8", errors='ignore')

	text = file.read();

	file.close()
	return text;
	
def writeToFile(filename, text):
	with open(filename, "a") as file:
		file.write(text);
		file.close();

def clearFiles(filelist, folder = ""):
	for filename in filelist:
		open(folder + filename, 'w').close();