import re;
#recursive function that extracts the data from the parse string to an array dataobject
#return list tells the parent that it needs to run several other queries

def extractDataFromParse(parse, returnlist = []):
	#remove empty data in front and at the end
	parse = parse.strip();

	key = "";
	endOfKey = 0;
	value = None;
	reachedEnd = False;
	nextStart = 0;
	#walk trough all values until a ( is found
	#breaks on success
	steps = -1;
	for i in range(0, len(parse)):
		if parse[i] != '(':
			continue;
		else:
			#keep track of how far into the sentence we are
			while True:
				#walk until we find the end of the first word
				steps += 1;
				char = parse[i+steps];
				if char.isspace():
					endOfKey = i+steps;
					key = parse[i+1: endOfKey]
					break;

			#check if it contains an array or it is a leaf
			maxopen = 0;
			childreturnsran= 0;
			
			openbrackets = 1;
			containsArray = False;
			closed = False;
			steps -= 1;
			while True:
				#if result is found break
				if isinstance(value, str):
					break;

				steps += 1;
				
				if i + steps >= len(parse):
					#occurs for root object
					break;
				
				char = parse[i+steps];
				#check if opened
				if char == "(":
					openbrackets += 1;
					maxopen += 1;
					containsArray = True;
					closed = False;
				
				if char == ")":
					openbrackets -= 1;
					
				# end situation reached
				if openbrackets == 0 and i + steps <= len(parse) and not closed:
					closed = True;
					if containsArray:
						#make sure we have an array to put data into
						if value == None:
							value = ["c"];
						returnlist.append(parse[endOfKey:i+steps+1]);
						while len(returnlist) != 0 and childreturnsran < maxopen:
							value.append(extractDataFromParse(returnlist.pop(0), returnlist));
							childreturnsran += 1;
						
						if(i+steps+1 > len(parse) - 5):
							reachedEnd = True;
						nextStart = i+steps;
						break;
					else:
						endOfValue = i + steps;
						if(endOfValue > len(parse) - 5):
							reachedEnd = True;
						else:
							nextStart = endOfValue;
						while True:
							steps -= 1;
							char = parse[i+steps];
							if char.isspace():
								value = parse[i + steps +1:endOfValue];
								steps = endOfValue - i + 1;
								break;
				elif openbrackets == -1 or steps + i >= len(parse):
					# closes another one so last of block
					break;
			break;
			
	if not reachedEnd:
		returnlist.append(parse[nextStart:]);
	return [key, value];

def getNamedEntities(parse, accepted):
	result = {};
	for sentence in parse['sentences']:
		for word in sentence['tokens']:
			if word['ner'] in accepted:
				if word['word'] in result:
					result[word['word']] += 1;
				else:
					result[word['word']] = 1;
				
	return result;	
					
	
def getTextCoreference(preprocessdata):
	
	clusters = preprocessdata['corefs'];
	sentences = preprocessdata['sentences'];	
	
	for cluster in clusters:
		chain = clusters[cluster];
		if len(chain) > 1:
			replacement = chain[0]['text'];
			for mention in chain:
				if True: #mention['text'] in ['he', 'she', 'it', 'He', 'She', 'It']:
					pos = mention['position'];
					#print(pos, mention['text'], sentences[pos[0] - 1]['tokens'][pos[1] - 1]['originalText']);
					sentence = sentences[pos[0] - 1]['tokens'];
					for word in sentence:
						if word['originalText'] == mention['text']:
							word['originalText'] = replacement;
	
	result = "";
	for sentence in sentences:
		currentsentence = "";
		for word in sentence['tokens']:
			currentsentence += word['before'] + word['originalText'] + word['after'];
		result += currentsentence;
	return result;
	
def printTree(tree, level = 0):
	if type(tree[1]) == list:
		#traverse tree
		print(' ' * level, tree[0]);
		for subtree in tree[1:]:
			#continue to the next layer
			printTree(subtree, level + 1);
	else:
		#leaf found: [0] type, [1] word
		print(' ' * level, tree);
	
def getSentenceIndexesWithNP(tree):
	sentence = 0;
	result = [];
	#traverse tree
	for subtree in tree[1:]:
		if '<NP>' in getSentenceRegex(subtree):
			result.append(sentence);
		sentence += 1;
	return result;


def hasNPHead(tree, requiredhead, parentTypes = '', first = False):
	#traverse tree
	if type(tree[1]) == list:
		if tree[0] != "c":
			parentTypes += "<"+tree[0]+">";
		
		first = True;
		for subtree in tree[1:]:
			if hasNPHead(subtree, requiredhead, parentTypes, first):
				return True;
			first = False;
	else:
		#leaf
		if '<NP>' in parentTypes:
			if first:
				if tree[1] == requiredhead:
					return True;
			else:
				if tree[0] in ['NN', 'NNS', 'NNP', 'NNPS'] and tree[1] == requiredhead:
					return True;
	
	return False;
	
def getWordCountRegex(tree, regex, parentTypes = "", result = {}):
	#traverse tree
	if type(tree[1]) == list:
		if tree[0] != "c":
			parentTypes += "<"+tree[0]+">";
			
		for subtree in tree[1:]:
			getWordCountRegex(subtree, regex, parentTypes, result);
	else:
		#leaf
		parentTypes += "<"+tree[0]+">";
		#if regexmatch
		found = re.search(regex, parentTypes);
		if found:
			if tree[1] in result:
				result[tree[1]] += 1;
			else:
				result[tree[1]] = 1;
	return result;

def getSentenceRegex(tree):
	result = "";
	#traverse tree
	if type(tree[1]) == list:
		if tree[0] != "c":
			result += "<"+tree[0]+">";
			
		for subtree in tree[1:]:
			result += getSentenceRegex(subtree);
	else:
		#leaf
		result += "<"+tree[0]+">";

	return result;
	
	
	
def getWordsRegex(tree, regex, parentTypes = "", result = []):
	#traverse tree
	if type(tree[1]) == list:
		if tree[0] != "c":
			parentTypes += "<"+tree[0]+">";
			
		for subtree in tree[1:]:
			getWordsRegex(subtree, regex, parentTypes, result);
	else:
		#leaf
		parentTypes += "<"+tree[0]+">";
		#if regexmatch
		found = re.search(regex, parentTypes);
		if found:
			result.append(tree[1]);
	return result;
	
	