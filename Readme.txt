Setup:
  install anaconda 32bit		https://www.continuum.io/downloads
  (contains pyton, scikit-learn and more libraries)
  pip install pycorenlp			https://github.com/smilli/py-corenlp
  download stanford coreNLP		http://nlp.stanford.edu/software/stanford-corenlp-full-2015-12-09.zip

Initial:
  Go to the stanford coreNLP folder and run:
	java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer
  test by going to webadress localhost:9000
  first run might take a while to boot and install additional components

Run program:
  Run "Python main.py"

Settings:
  modify settings with the settings.txt file
  all combinations will be tested so it can take a while