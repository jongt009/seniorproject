Gum for Health.
Want some dessert, but you�re trying to lose
weight?
Want to get lots of vitamins, but don�t like
eating fruits and vegetables?
Want whiter teeth?
Try chewing some gum.
Gum manufacturers are trying all sorts of new
angles to get people to buy more gum.
Combining gum with healthy benefits such as
vitamins or tooth whiteners gets people�s
attention so they are more likely to buy the
product.
Gum sales tend to stay fairly steady. Some
people don�t like chewing gum, and the people
who do chew gum can only chew so much gum
in a day.
�The only way to sell more gum is to constantly
be coming up with something new,� said David
Johnston, Marketing Director for a major gum
manufacturer. �A great deal of research goes
into each new product. We plan the flavor and
other aspects of the gum so that they appeal to
certain groups of people. For example, sour
flavors tend to appeal to younger people. Older
people often prefer more traditional flavors like
cinnamon or mint.�
Even the color of the gum is chosen because of
how it will attract certain people�s attention.
For many people, the flavor is the most
important reason for choosing one gum over
another. Companies are routinely releasing
new flavors.
�Tropical flavors are big right now,� explained
Mr. Johnston, �especially with younger gum
chewers.�
Another current trend is flavor blends, such as
citrus with strawberry or berry with green
apple.
To take the flavor experience one step further,
Kraft Foods created a gum called Stride Shift.
This gum actually changes flavor as you chew it.
It starts out with a fruit flavor and shifts to mint.
Another gum company has created a line of
flavors designed to taste like your favorite
sweet treat. Wrigley�s Extra Dessert Delight
comes in flavors like Key Lime Pie, Strawberry
Shortcake and Chocolate Chip Mint. This gum is
marketed to dieters as a way to get the sweet,
yummy flavor of a dessert without the calories
of the real thing.
�Gum can be great for helping lose weight,�
said Jane Mason, a mother of two and frequent
gum chewer. �I chew a piece of sugar-free gum
every night after dinner, especially while I�m
watching TV with my family. It signals to my
brain that I�m done eating for the day, but I 

don�t feel like I�m missing out on having a
snack.�
Other gum companies are focusing on health
benefits to make their gum stand out. Several
companies sell gum that claims to whiten teeth.
Trident�s Vitality, one of the newest on the
market, contains vitamin C.
There is some evidence that gum chewing can
also help memory, but it appears as though the
benefit comes from the act of chewing the gum,
not from ingredients in the gum. This means
that any gum might work.
�Memory is closely tied to scent in our minds,�
explained Mr. Johnston. According to the
research, if you chew a certain flavor of gum
while you study for a test and then chew that
same flavor while you take the test, you�ll be
better able to remember what you studied.
�Our green apple flavor seems to work
particularly well,� says Mr. Johnston, �but we
don�t say that on the label. There hasn�t been
enough research done to prove how much of a
difference it makes. There are many people,
however, who claim it works for them.�
Gum may also help with memory by increasing
oxygen flow to the brain. The physical act of
chewing seems to bring more oxygen into the
nose and mouth, which, in turn, benefits the
rest of the body.
The oxygen may help some people feel more
awake. �When I�m studying late at night I chew
gum,� said high school student Rebecca
Brigham. �I swear it makes me feel less tired.�
Of course, if you are really concerned about
staying awake, you can try one of the several
gums with caffeine as an added ingredient.
If the flavors and added benefits don�t get your
attention, maybe the packaging will. �We�ve
made our packages of gum bigger,� said David
Johnston. �We also redesigned the package to
make it less likely for the gum to fall out in a
woman�s purse.�
�We want to create something that generates
impulse buys. A person sees our gum at the
grocery store check stand, it gets their
attention, and they want to buy it. That�s what
we�re going for.�
So is it really a good idea to chew gum?
As long as the gum is sugar-free. Chewing
sugar-free gum after meals can help clean the
teeth, reducing the chance of cavities. Gum is
well known as a way to fix bad breath. And the
action of chewing the gum can help strengthen
teeth.
�My dentist tells me every visit that I should be
chewing at least two pieces of sugar-free gum
every day,� said Jane Mason. �I don�t know if
every dentist says that, but I�m not going to
complain!�