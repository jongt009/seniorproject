Driving with Pets.
Sandy Michelson loves her mini terrier,
Marshmallow. He�s part of her family. �He�s
my baby,� she said with a smile. She held the
small dog in her arms. �I take him with me
everywhere.�
Sandy often drove her car with Marshmallow on
her lap. �He loved sticking his head out the
window. It was so cute.� She thought it was
safe since he was so small. �I could still reach
the steering wheel. I thought if something
happened, I�d just hold onto him. He�d be fine.�
She was wrong.
On a Saturday afternoon, Sandy was taking
Marshmallow to get a haircut. �It had been
rainy. The roads were a little slick.
Marshmallow was on my lap. I put one hand on
his back to hold him still. Then I used the other
hand to turn into the parking lot. I didn�t notice
that there was a truck behind me.�
The truck�s driver didn�t realize Sandy had
slowed down and started to turn. It was too
late to stop on the wet roads. The truck hit the
back of Sandy�s car.
Sandy�s car flew forward. The airbag deployed,
It kept her from having a big injury. �I got some
cuts and scrapes. I got whiplash.� Whiplash is
damage to the neck muscles from the sudden
crash.
�Marshmallow is so small. But I couldn�t hold
on to him.� The airbag hit the dog�s back legs�
It sent him flying into the side of the door. Both
of his back legs were broken.
�He�s getting better,� said Sandy. She touched
the casts on Marshmallow�s legs. �But he�s in
pain a lot. He needs help going to the
bathroom. And all those doctor bills! It�s going
to take me a long time to pay for it.�
Sandy was given a ticket for not using a turn
signal.
Recently in the news, driving with distractions
has been a popular topic. In many places it is
illegal to send text messages while driving. In
some places it is illegal to talk on a cell phone
while driving. Drivers who are distracted can
have as big of a problem as someone driving
drunk.
But what about driving with pets?
�Sadly, we see it all the time,� says Officer Terry
Ferguson from the county sheriff�s office.
�People don�t seem to think anything is wrong
with having a pet free in their car.�
No one is exactly sure how many accidents are
caused each year because of pets in vehicles.
But one study estimates that there more than
20,000 of these accidents every year across the
country.
�Pets can distract the driver in so many ways,�
explained Officer Ferguson. �The animal may
start barking. It may go to the bathroom. It
may start chewing on something. The driver�s 
attention goes to the animal. This means the
driver is not paying attention to driving.�
In some cases, the pet just simply gets in the
way. �Maybe the driver can�t turn the steering
wheel because the pet is in the way. Or the pet
may block the driver�s view. Once a cat got
scared by car horn. The cat tried to hide under
the car�s pedals on the floor. The driver
couldn�t break with the cat under the pedal.
They ended up crashing into a tree.
When a driver is behind the wheel, he or she
needs to do their very best to pay attention to
the road. In 2009 over 5,400 people were killed
in accidents caused by a distracted driver.
Almost 450,000 were injured.
Only one state, Hawaii, has a law that forbids
drivers from having an animal on their lap.
Other states and cities are considering similar
laws.
�We did a big promotion last summer. We
handed out pet restraint systems for free.
Basically the system works like a seatbelt for
the pet. But people gave all sorts of excuses as
to why they didn�t think they needed it. People
said their pets were well behaved. They said
the animal wouldn�t like it. Or they think that
their pet was so small that it didn�t matter.�
In reality, an animal in a car that stops suddenly
flies through the vehicle like a cannon ball.
Imagine a car moving 35 miles an hour crashes.
A 50 pound animal would be thrown forward
with 1,500 pounds of force.
�Last summer there was an accident. A dog in
the back of a pickup truck was killed from a
crash. It was a tragedy. But it could have been
easily prevented,� said Officer Ferguson.
�I never go anywhere now with Marshmallow
without having his safety restraint on in the car.
At first he whined about it. But I told him it was
for his own good,� said Sandy. �I love him too
much not to try to keep him safe.�