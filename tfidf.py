import nltk
import string
import os
import math

from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.porter import PorterStemmer
from bs4 import BeautifulSoup,SoupStrainer

txtpath = './datafiles/tfidfset/txt/'
sgmpath = './datafiles/tfidfset/sgm/'
token_dict = {}
stemmer = PorterStemmer()

def stem_tokens(tokens, stemmer):
	stemmed = []
	for item in tokens:
		stemmed.append(stemmer.stem(item))
	return stemmed

def tokenize(text):
	tokens = nltk.word_tokenize(text)
	stems = stem_tokens(tokens, stemmer)
	cleanedStems = [];
	for word in stems:
		cleanedStems.append(word.replace('+', '').replace(' ', '').replace(',','').replace('.',''));
	return stems

for subdir, dirs, files in os.walk(txtpath):
	for file in files:
		file_path = subdir + os.path.sep + file
		fileread = open(file_path, 'r')
		text = fileread.read()
		lowers = text.lower().replace(u"\u2018", "'").replace(u"\u2019", "'") # also remove left and right quote characters
		lowers = lowers.replace(u'\u201C', "\"").replace(u'\u201D', "\"").replace("–", "-")
		no_punctuation = lowers.translate(string.punctuation)
		token_dict[file_path] = no_punctuation

print("Done loading TXT");

if(True):

	filenumid = 0;
	for subdir, dirs, files in os.walk(sgmpath):
		for file in files:
			file_path = subdir + os.path.sep + file
			
			f = open(file_path, 'r')
			data= f.read()
			soup = BeautifulSoup(data, "lxml")
			contents = soup.findAll('content')
			for content in contents:
				filenumid += 1;
				no_punctuation = content.text.translate(string.punctuation)
				token_dict[file_path + str(filenumid)] = no_punctuation
			
	print("Done loading SGM")		
else:
	print('Skipped loading SGM');
#this can take some time
tfidf = TfidfVectorizer(ngram_range=(1, 1), tokenizer=None, stop_words='english')
tfs = tfidf.fit_transform(token_dict.values())

feature_names = tfidf.get_feature_names();

wordlist = {};
i = 0;


for word in feature_names:
	wordlist[word] = i;
	i += 1;

def getScoreList(word):
	if word in wordlist:
		return tfs.getcol(wordlist[word]);
	else:
		return [[0],[]]


def getWidth(word):
	return len(getScoreList(word).nonzero()[0]);

def getScore(word):
	stemmedword = stemmer.stem(word.lower());
	stemmedword = stemmedword.replace('+', '').replace(' ', '').replace(',','').replace('.','');
	if stemmedword in wordlist:
		return getScoreList(stemmedword).mean() * math.log(1000000 / getWidth(stemmedword));
	return 0;