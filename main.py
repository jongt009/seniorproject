from stanfordreader import *
from settings import *
from readfile import *

import winsound

def main():
	s = Settings("settings.txt");
	s.loadSettings();
	clearFiles(s.settingsDictionary['file'], "output/");
	clearFiles(['output/rawresults.txt', 'output/processedresults.txt'], "");
	
	while s.nextSetting():
		print("\n", Settings.current)
		run();

main();

winsound.Beep(500,1000);
