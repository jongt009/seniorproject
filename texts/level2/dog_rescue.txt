Dog Rescue.
After a rain storm last summer, Emily Bennett
noticed dog she didn�t recognize wandering
around her neighborhood. The dog was wet
and tired, but healthy.
�The dog had a collar, but no tags. He looked
well fed. My mom thought he was probably
someone�s pet. She thought he got scared and
ran away during the storm.� Emily wanted to
help the dog get back to his owner.
�My mom called the local animal shelters. But
no one had been looking for a dog that looked
like the one we found.�
The Bennett family was willing to keep the dog.
But Emily knew it really belonged to someone
else. �It made me sad to think that some other
kid out there was missing his dog.� They called
the dog Stormy.
A week later, Emily�s mom called the local
shelters again. This time there was a match. A
family had been looking for a dog that matched
Stormy�s description. After a few more phone
calls, the two families met at the shelter.
Stormy was reunited with his family.
�It was sad to say goodbye to Stormy,� said
Emily. �But his family looked so happy to have
him back. I was really glad they had found each
other again.�
Anne Wilson runs one of the local shelters. She
told Emily about the other dogs that were
found and turned in after the storm. �Every
time there is a big storm with a lot of lightning
and thunder, we end up with at least three or
four lost dogs coming to us. Dogs are often
scared of the noise of the thunder, so they run
away and end up lost.�
�Most of the time,� Miss Wilson explained, �the
dog�s owners come here and find their dog. But
every year there are at least two or three that
end up staying with us. They never make it
back to their family.�
Even after Stormy was back at home with his
family, Emily couldn�t stop thinking about him.
She thought of the other dogs that had been
lost. The weather forecast was calling for more
storms.
�Emily was very upset by the idea that more
dogs would end up lost,� said Emily�s mom.
�She wanted to find a way to make it easier for
owners to find their lost pets.�
Emily had an idea. Her father is a computer
programmer. He helped Emily create a new
website. On the website, Emily posts photos of
all of the dogs that are currently staying at each
of the local shelters. �This way,� she explains,
�there is one place where a family can go to see
if their dog has been found.�
Anne Wilson and the other shelters owners�
loved the idea. �We try to keep in contact with
each other. But until now it�s been the pet
owner�s responsibility to call each shelter and
ask if their dog has been found and turned in 
there. It�s a hard process for the pets� owners.
They are often told many times that they�re dog
isn�t there before they find the right shelter.�
�Dogs have been known to run many miles
when they are scared. Sometimes the dogs end
up at a shelter that is a long way from their
homes. Their owners don�t realize it�s possible
that the dog could have ended up so far away.�
Whenever a new lost dog comes into the
shelter, a person at the shelter takes a picture
of the dog. The picture is sent in an e-mail to
Emily and her dad. The e-mail also includes a
description of the dog that tells the gender,
color and other identifying features. Emily and
her dad post the dog�s picture on their website
that same day.
�Now when someone calls to ask about a lost
pet,� said Miss Wilson, �and if the pet isn�t here
at our shelter, we tell the family to check the
website.�
In the six months that the website has been
active, only one of the dogs featured has not
been reunited with its family.
�It�s amazing to see what a difference it makes,�
Miss Wilson said. �It�s so sad when lost dogs
and owners can�t find each other. But now the
process is much simpler. I�m so proud of what
Emily and her family are doing to help.�
Emily has a collection of thank you notes. She�s
received them from the owners of dogs who
have been found through her website. She
holds up one written with scribbled crayon.
�This little girl�s puppy ran away during a storm
a couple weeks ago. The puppy ended up at a
shelter. Then we put the picture on the
website. The owners saw it and got the puppy
back all within one day.� Emily smiles. �It
makes me happy that I could help this little girl
get her puppy back.�
Miss Wilson hopes that many more dogs will be
reunited with their families. She also hopes
that more families take precautions so that
their pets don�t get lost in the first place. She
offers these tips for keeping your dog safe:
 1. Make sure your dog wears a collar with
tags. The tags should include your name and
phone number so you can be contacted if the
dog is found.
 2. During a storm, allow your dog to come
inside the house or make sure that the dog has
a dog house or porch. Give him a place where
he can feel safe and protected.
 3. Put a fence around your yard so your
dog has a safe place to play but can�t get out
and run away.